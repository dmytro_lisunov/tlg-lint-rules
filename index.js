'use strict';

let path = require('path');

const configs = {
    'stylelint': path.join(__dirname, '.stylelintrc.json'),
    'eslint': path.join(__dirname, '.eslintrc.json'),
    'prettier': path.join(__dirname, '.prettierrc.json'),
}

module.exports = configs;